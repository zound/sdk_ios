# Controller SDK
## /Development/TAPlatform.framework
    The Controller SDK for development environment, support both device & simulator, this is a fat library
## /Production/TAPlatform.framework
    The Controller SDK for production environment, only support device

# com.tymphany.controllersdk.ControllerSDK.docset
## /Contents/Resources/Documents/index.html
    The document of Controller SDK in html

# Controller SDK Release Notes.pdf
    The release notes of Controller SDK

# Controller SDK programming guide x.x.docx
    The simple guide helps to implement the BLE feature with Controller SDK

# iOS_Controller_SDK_Reference_App_User_Guide_vx.xx
The user guide of the Controller SDK reference App

# qcc3008.plist
    The config file for Controller SDK, see <Controller SDK programming guide>
