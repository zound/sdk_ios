//
//  TAPDigitalSignalProcessingService.h
//  TAPDigitalSignalProcessingService
//
//  Created by Lam Yick Hong on 20/4/15.
//  Copyright (c) 2015 Tymphany. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAPService.h"

FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyLowPassFrequency;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyLowPassSlope;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyPhase;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyPolarity;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ1Frequency;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ1Boost;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ1QFactor;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ2Frequency;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ2Boost;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ2QFactor;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ3Frequency;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ3Boost;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyEQ3QFactor;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyRGCFrequency;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyRGCSlope;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyLowPassOnOff;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyPEQ1OnOff;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyPEQ2OnOff;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyPEQ3OnOff;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyRGCOnOff;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyVolume;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyDisplay;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyTimeout;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyStandby;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyBrightness;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyTunning;

FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingCompletionKeyFullSettings;

FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyToneEQX;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyToneEQY;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyToneEQZ;

FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyGraphicalEQBass;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyGraphicalEQLow;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyGraphicalEQMid;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyGraphicalEQUpper;
FOUNDATION_EXPORT NSString * const TAPDigitalSignalProcessingKeyGraphicalEQHigh;

/*!
 *  Values representing step of EQ toggle button
 */
typedef NS_ENUM(NSInteger, TAPDigitalSignalProcessingServiceEQButtonStep){
    /*!
     *  Step 1 of EQ toggle button
     */
    TAPDigitalSignalProcessingServiceEQButtonStep1 = 0,
    /*!
     *  Step 2 of EQ toggle button
     */
    TAPDigitalSignalProcessingServiceEQButtonStep2,
    /*!
     *  Step 2 of EQ toggle button
     */
    TAPDigitalSignalProcessingServiceEQButtonStep3,
    /*!
     *  Unknown step of EQ toggle button
     */
    TAPDigitalSignalProcessingServiceEQButtonStepUnknown
};

/*!
 *  Values representing EQ settings style
 */
typedef NS_ENUM(NSInteger, TAPDigitalSignalProcessingServiceEQPreset){
    /*!
     *  Flat Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetFlat = 0,
    /*!
     *  Custom Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetCustom,
    /*!
     *  Rock Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetRock,
    /*!
     *  Metal Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetMetal,
    /*!
     *  Pop Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetPop,
    /*!
     *  Hip-Hop Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetHipHop,
    /*!
     *  Electronic Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetElectronic,
    /*!
     *  Jazz Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetJazz,
    /*!
     *  Unknown Equalizer style
     */
    TAPDigitalSignalProcessingServiceEQPresetUnknown
};

@protocol TAPDigitalSignalProcessingServiceDelegate;

/*!
 *
 *  @interface TAPDigitalSignalProcessingService
 *  @brief     TAP Service for controlling the digital signal processing feature. Helps exchange DSP related information.
 *  @author    Hong Lam
 *  @date      20/4/15
 *  @copyright Tymphany Ltd.
 *
 *  @discussion     TAP Service for controlling the digital signal processing feature. Helps exchange DSP related information.
 *
 *  [TAPDigitalSignalProcessingService](#) objects are used to control the Digital Signal Processing(DSP) function on the system.
 *
 *  You should implement the [TAPDigitalSignalProcessingServiceDelegate](#) and assign to the TAPDigitalSignalProcessingService object to receive any status update related to the DSP.
 *
 */
@interface TAPDigitalSignalProcessingService : TAPService

/*!
 *  @property   delegate
 *  @discussion A TAPDigitalSignalProcessingService delegate object
 *  @see    TAPDigitalSignalProcessingServiceDelegate
 */
@property (nonatomic, strong) id<TAPDigitalSignalProcessingServiceDelegate> delegate;

/*!
 *  @method system:equalizer:
 *
 *  @param system           The target system
 *  @param equalizer        The block which taking the equalizer as a parameter
 *  @brief                  Reads the equalizer from the target system
 *
 *  @discussion On BLE, the complete block will return an NSDictionary object consist of the current equalizer settings info.
 */
- (void)system:(id)system equalizer:(void (^)(NSDictionary*, NSError*))equalizer;

/*!
 *  @method system:equalizer:
 *
 *  @param system           The target system
 *  @param equalizer        The block which taking the equalizer as a parameter
 *  @brief                  Reads the equalizer from the target system
 *
 *  @discussion On BLE, the complete block will return an NSDictionary object consist of the current equalizer settings info.
 */
- (void)system:(id)system targetKeys:(NSArray*)targetKeys equalizer:(void (^)(NSDictionary*, NSError*))equalizer;

/*!
 *  @method system:writeEqualizerType:completion:
 *
 *  @param system           The target system
 *  @param type             The type of value to be written
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the equalizer to the target system
 *
 *
 */
- (void)system:(TAPSystem*)system writeEqualizerType:(NSString*)type value:(id)value completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorEqualizerOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Starts monitoring the equalizer of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE equalizer charateristic provided by the system in audio service. Whenever there's a equalizer update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPDigitalSignalProcessingService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPDigitalSignalProcessingService](#) , [TAPDigitalSignalProcessingServiceDelegate system:didUpdateEqualizer:] will be triggered and provide the response data.
 *
 *  @see [TAPDigitalSignalProcessingServiceDelegate system:didUpdateEqualizer:]
 */
- (void)startMonitorEqualizerOfSystem:(id)system;

/*!
 *  @method stopMonitorEqualizerOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the equalizer info of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscribe to the BLE equalizer charateristic provided by the system in audio service.
 *  @see  startMonitorEqualizerOfSystem:
 */
- (void)stopMonitorEqualizerOfSystem:(id)system;

/*!
 *  @method system:resetEqualizerType:completion:
 *
 *  @param system           The target system
 *  @param type             The type of value to be reset
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error, use key TAPDigitalSignalProcessingCompletionKeyFullSettings to extract the full settings dictionary
 *
 *  @brief                  reset the equalizer to the target system
 *
 */
- (void)system:(TAPSystem*)system resetEqualizerType:(NSString*)type completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:toneEQ:
 *
 *  @param system           The target system
 *  @param equalizer        The block which taking the tone equalizer dictionary as a parameter, use TAPDigitalSignalProcessingKeyToneEQX, TAPDigitalSignalProcessingKeyToneEQY, TAPDigitalSignalProcessingKeyToneEQY to extract the value of x,y,z
 *
 *  @brief                  Reads the tone equalizer from the target system
 *
 *
 */
- (void)system:(id)system toneEqualizer:(void (^)(NSDictionary*))equalizer;//TODO: to be implemented

/*!
 *  @method system:writeToneEqualizerWithX:Y:Z:completion
 *
 *  @param system           The target system
 *  @param x                The parameter X
 *  @param y                The parameter Y
 *  @param z                The parameter Z
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the tone equalizer to the target system
 *
 *
 */
- (void)system:(id)system writeToneEqualizerWithX:(NSInteger)x Y:(NSInteger)y Z:(NSInteger)z completion:(void (^)(NSDictionary*))complete;//TODO: to be implemented

/*!
 *  @method startMonitorToneEqualizerOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Starts monitoring the tone equalizer of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE tone equalizer charateristic provided by the system in audio service. Whenever there's a tone equalizer update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPDigitalSignalProcessingService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPDigitalSignalProcessingService](#) , [TAPDigitalSignalProcessingServiceDelegate system:didUpdateToneEqualizer:] will be triggered and provide the response data.
 *
 *  @see [TAPDigitalSignalProcessingServiceDelegate system:didUpdateToneEqualizer:]
 */
- (void)startMonitorToneEqualizerOfSystem:(id)system;//TODO: to be implemented

/*!
 *  @method stopMonitorToneEqualizerOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the tone equalizer info of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscribe to the BLE tone equalizer charateristic provided by the system in audio service.
 *  @see startMonitorToneEqualizerOfSystem:
 */
- (void)stopMonitorToneEqualizerOfSystem:(id)system;//TODO: to be implemented

/*!
 *  @method system:graphicalEqualizer:
 *
 *  @param system           The target system
 *  @param equalizer        The block which taking the graphical equalizer dictionary as a parameter
 *
 *  @brief                  Reads the graphical equalizer from the target system
 *
 *  @discussion Use keys TAPDigitalSignalProcessingKeyGraphicalEQBass. TAPDigitalSignalProcessingKeyGraphicalEQLow, TAPDigitalSignalProcessingKeyGraphicalEQMid, TAPDigitalSignalProcessingKeyGraphicalEQUpper, TAPDigitalSignalProcessingKeyGraphicalEQHigh to extract the equalizer gain in integer value of bass, low, mid, upper, high frequence
 */
- (void)system:(id)system graphicalEqualizer:(void (^)(NSDictionary*))equalizer;

/*!
 *  @method system:writeGraphicalEqualizerWithBass:low:mid:upper:high:completion
 *
 *  @param system           The target system
 *  @param bass             The gain of bass frequency, range in [0,255]
 *  @param low              The gain of low frequency, range in [0,255]
 *  @param mid              The gain of mid frequency, range in [0,255]
 *  @param upper            The gain of upper frequency, range in [0,255]
 *  @param high             The gain of high frequency, range in [0,255]
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the graphical equalizer to the target system
 *
 *
 */
- (void)system:(id)system writeGraphicalEqualizerWithBass:(NSInteger)bass low:(NSInteger)low mid:(NSInteger)mid upper:(NSInteger)upper high:(NSInteger)high completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorGraphicalEqualizerOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Starts monitoring the graphical equalizer of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE graphical equalizer charateristic provided by the system in audio service. Whenever there's a graphical equalizer update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPDigitalSignalProcessingService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPDigitalSignalProcessingService](#) , [TAPDigitalSignalProcessingServiceDelegate system:didUpdateGraphicalEqualizer:] will be triggered and provide the response data.
 *
 *  @see [TAPDigitalSignalProcessingServiceDelegate system:didUpdateGraphicalEqualizer:]
 */
- (void)startMonitorGraphicalEqualizerOfSystem:(id)system;

/*!
 *  @method stopMonitorGraphicalEqualizerOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the graphical equalizer info of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscribe to the BLE graphical equalizer charateristic provided by the system in audio service.
 *  @see startMonitorGraphicalEqualizerOfSystem:
 */
- (void)stopMonitorGraphicalEqualizerOfSystem:(id)system;

/*!
 *  @method system:EQSettings:
 *
 *  @param system           The target system
 *  @param settings         The block which taking current EQ button step, EQ preset of EQ button step 1 and EQ preset of EQ button step 2 as a parameter
 *
 *  @brief                  Read the EQ  settings of the target system
 *  @see                    TAPDigitalSignalProcessingServiceEQButtonStep
 *  @see                    TAPDigitalSignalProcessingServiceEQPreset
 *
 */
- (void)system:(id)system EQSettings:(void (^)(TAPDigitalSignalProcessingServiceEQButtonStep, TAPDigitalSignalProcessingServiceEQPreset, TAPDigitalSignalProcessingServiceEQPreset))settings;

/*!
 *  @method system:switchToEQButtonStep:completion:
 *
 *  @param system           The target system
 *  @param step             The EQ button step
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Switch EQ button step of the target system
 *  @see                    TAPDigitalSignalProcessingServiceEQButtonStep
 *
 */
- (void)system:(id)system switchToEQButtonStep:(TAPDigitalSignalProcessingServiceEQButtonStep)step completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:loadEQPreset:toEQButtonStep:completion:
 *
 *  @param system           The target system
 *  @param preset           The EQ preset
 *  @param step             The target EQ button step
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Load EQ preset to target EQ button step of the target system
 *  @see                    TAPDigitalSignalProcessingServiceEQButtonStep
 *
 */
- (void)system:(id)system loadEQPreset:(TAPDigitalSignalProcessingServiceEQPreset)preset toEQButtonStep:(TAPDigitalSignalProcessingServiceEQButtonStep)step completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:writeEQButtonStep2WithPreset:completion:
 *
 *  @param system           The target system
 *  @param preset           The EQ preset
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write EQ preset to step2 of the target system. This method is provided for the ease of specific project.
 *  @see                    TAPDigitalSignalProcessingServiceEQButtonStep
 *
 */
- (void)system:(id)system writeEQButtonStep2WithPreset:(TAPDigitalSignalProcessingServiceEQPreset)preset completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:writeEQButtonStep3WithPreset:completion:
 *
 *  @param system           The target system
 *  @param preset           The EQ preset
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write EQ preset to step3 of the target system. This method is provided for the ease of specific project.
 *  @see                    TAPDigitalSignalProcessingServiceEQButtonStep
 *
 */
- (void)system:(id)system writeEQButtonStep3WithPreset:(TAPDigitalSignalProcessingServiceEQPreset)preset completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:customPresetGraphicalEqualizer:
 *
 *  @param system           The target system
 *  @param equalizer        The block which taking the graphical equalizer dictionary of custom preset as a parameter
 *
 *  @brief                  Reads the graphichal equalizer of custom preset from the target system
 *
 *  @discussion Use keys TAPDigitalSignalProcessingKeyGraphicalEQBass. TAPDigitalSignalProcessingKeyGraphicalEQLow, TAPDigitalSignalProcessingKeyGraphicalEQMid, TAPDigitalSignalProcessingKeyGraphicalEQUpper, TAPDigitalSignalProcessingKeyGraphicalEQHigh to extract the equalizer gain in integer value of bass, low, mid, upper, high frequence
 */
- (void)system:(id)system customPresetGraphicalEqualizer:(void (^)(NSDictionary*))equalizer;

/*!
 *  @method system:writeGraphicalEqualizerToCustomPresetWithBass:low:mid:upper:high:completion
 *
 *  @param system           The target system
 *  @param bass             The gain of bass frequency, range in [0,255]
 *  @param low              The gain of low frequency, range in [0,255]
 *  @param mid              The gain of mid frequency, range in [0,255]
 *  @param upper            The gain of upper frequency, range in [0,255]
 *  @param high             The gain of high frequency, range in [0,255]
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the graphical equalizer to custom preset of the target system
 *
 */
- (void)system:(id)system writeGraphicalEqualizerToCustomPresetWithBass:(NSInteger)bass low:(NSInteger)low mid:(NSInteger)mid upper:(NSInteger)upper high:(NSInteger)high completion:(void (^)(NSDictionary*))complete;
/*!
 *  @method startMonitorEQButtonOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Starts monitoring the EQ button of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE EQ settings charateristic provided by the system in audio service. Whenever there's a EQ button step update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPDigitalSignalProcessingService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPDigitalSignalProcessingService](#) , [TAPDigitalSignalProcessingServiceDelegate system:didUpdateEQButtonStep:] will be triggered and provide the response data.
 *
 *  @see [TAPDigitalSignalProcessingServiceDelegate system:didUpdateEQButtonStep:]
 */
- (void)startMonitorEQButtonOfSystem:(id)system;

/*!
 *  @method stopMonitorEQButtonOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring EQ button status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscribe to the BLE EQ settings charateristic provided by the system in audio service.
 *  @see startMonitorEQButtonOfSystem:
 */
- (void)stopMonitorEQButtonOfSystem:(id)system;

@end

/*!
 *  @protocol TAPDigitalSignalProcessingServiceDelegate
 *
 *  @brief    Delegate define methods for receive notifications from TAPDigitalSignalProcessingService object.
 *
 */
@protocol TAPDigitalSignalProcessingServiceDelegate <NSObject>

@required

@optional

/*!
 *  @method system:didUpdateEqualizer:
 *
 *  @param system       The system which has this update
 *  @param equalizer    The new equalizer
 *
 *  @brief              Invoked when a the system has a equalizer update
 */
- (void)system:(id)system didUpdateEqualizer:(NSDictionary*)equalizer;

/*!
 *  @method system:didUpdateToneEqualizer:
 *
 *  @param system       The system which has this update
 *  @param equalizer    The new tone equalizer dictionary, use TAPDigitalSignalProcessingKeyToneEQX, TAPDigitalSignalProcessingKeyToneEQY, TAPDigitalSignalProcessingKeyToneEQY to extract the value of x,y,z
 *
 *  @brief              Invoked when a the system has a tone equalizer update
 */
- (void)system:(id)system didUpdateToneEqualizer:(NSDictionary*)equalizer;//TODO: to be implemented

/*!
 *  @method system:didUpdateGraphicalEqualizer:
 *
 *  @param system       The system which has this update
 *  @param equalizer        The block which taking the graphical equalizer dictionary as a parameter
 *
 *  @brief              Invoked when a the system has a graphical equalizer update
 *
 *  @discussion Use keys TAPDigitalSignalProcessingKeyGraphicalEQBass, TAPDigitalSignalProcessingKeyGraphicalEQLow, TAPDigitalSignalProcessingKeyGraphicalEQMid, TAPDigitalSignalProcessingKeyGraphicalEQUpper, TAPDigitalSignalProcessingKeyGraphicalEQHigh to extract the equalizer gain in integer value of bass, low, mid, upper, high frequence
 */
- (void)system:(id)system didUpdateGraphicalEqualizer:(NSDictionary*)equalizer;

/*!
 *  @method system:didUpdateEQButtonStep:
 *
 *  @param system       The system which has this update
 *  @param step         The new EQ button step
 *
 *  @brief              Invoked when a the system has a EQ button step update
 *  @see                TAPDigitalSignalProcessingServiceEQButtonStep
 */
- (void)system:(id)system didUpdateEQButtonStep:(TAPDigitalSignalProcessingServiceEQButtonStep)step;
@end
