//
//  TALogger.h
//  TAPlatform
//
//  Created by Lam Yick Hong on 15/9/15.
//  Copyright (c) 2015 Tymphany. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *
 *  @interface TAPSystem
 *  @brief     TAP logger
 *  @author    Hong Lam
 *  @copyright Tymphany Ltd.
 *
 *
 *  @discussion This class provided methods to obtain the log data.
 *
 *  The log level of TAP in production build is higher than that in development build. In production environment, TAP logger will only log warning and error. While it will log more detailed information in development environment.
 *
 */
@interface TAPLogger : NSObject


/*!
 *  @method sharedLogger
 *
 *  @return The TAP logger
 */
+ (TAPLogger*)sharedLogger;

/*!
 *  @method getLogfile:
 *
 *  @return The SDK log in data
 *
 *  @discussion Generate log text with the data
 *
 */
- (NSData*)getLogfile;

@end

