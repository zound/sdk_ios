//
//  TAPFirmwareOverTheAirUpdateService.h
//  TAPlatform
//
//  Created by Alain Hsu on 24/01/2018.
//  Copyright © 2018 Tymphany. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAPService.h"

/*!
 *  Values representing the current OTA status
 */
typedef NS_ENUM(NSInteger, TAPFirmwareOTAUpdateServiceOTAStatus){
    /*!
     *  The states where the system is not in OTA
     */
    TAPFirmwareOTAUpdateServiceOTAStatusNotStarted = 0,
    /*!
     *  The states where the system is ready for OTA
     */
    TAPFirmwareOTAUpdateServiceOTAStatusOTAReady,
    /*!
     *  The states where the system is receiving OTA file
     */
    TAPFirmwareOTAUpdateServiceOTAStatusDownloading,
    /*!
     *  The states where the system finish receiving OTA file
     */
    TAPFirmwareOTAUpdateServiceOTAStatusDownloadFinished,
    /*!
     *  The states where the system finish OTA validation
     */
    TAPFirmwareOTAUpdateServiceOTAStatusValidated,    
    /*!
     *  The states where the system is activating
     */
    TAPFirmwareOTAUpdateServiceOTAStatusActivating,
    /*!
     *  The states where the system finish Bluetooth firmware update
     */
    TAPFirmwareOTAUpdateServiceOTAStatusBTUpgradeFinish,
    /*!
     *  The states where the system is upgrading MCU
     */
    TAPFirmwareOTAUpdateServiceOTAStatusMCUUpgrading,
    /*!
     *  The states where the system finish MCU firmware update
     */
    TAPFirmwareOTAUpdateServiceOTAStatusMCUUpgradeFinish,
    /*!
     *  The states where the system is paused in OTA
     */
    TAPFirmwareOTAUpdateServiceOTAStatusPaused,
    /*!
     *  The states where the system receive error and stop OTA
     */
    TAPFirmwareOTAUpdateServiceOTAStatusError = 255
};

@protocol TAPFirmwareOverTheAirUpdateServiceDelegate;

/*!
 *
 *  @interface TAPFirmwareOverTheAirUpdateService
 *  @brief     TAP Service for controlling the firmware OTA update.
 *  @author    Alain Hsu
 *  @date      5/2/18
 *  @copyright Tymphany Ltd.
 *
 *  @discussion     TAP Service for controlling the firmware OTA update.
 *
 *  [TAPFirmwareOverTheAirUpdateService](#) objects are used to control the firmware OTA update function on the system.
 *
 *  You should implement the [TAPFirmwareOverTheAirUpdateServiceDelegate](#) and assign to the [TAPFirmwareOverTheAirUpdateService](#) object to receive any status update related to OTA update.
 */
@interface TAPFirmwareOverTheAirUpdateService : TAPService

/*!
 *  @property   delegate
 *  @brief      A TAPFirmwareOverTheAirUpdateServiceDelegate delegate object
 *  @see        TAPFirmwareOverTheAirUpdateServiceDelegate
 */
@property (nonatomic, strong) id<TAPFirmwareOverTheAirUpdateServiceDelegate> delegate;

/*!
 *  @method system:checkOTAStatus:
 *
 *  @param system       The system which has this update
 *  @param complete     The completion block returns OTA status, the updated version string and error object
 *
 *  @brief              Read OTA status of target system
 *  @discussion         For the returning completion block, the updated version is the version of system to be updated, it is valid in all OTA status except TAPFirmwareOTAUpdateServiceOTAStatusNotStarted and TAPFirmwareOTAUpdateServiceOTAStatusError, error is valid only in TAPFirmwareOTAUpdateServiceOTAStatusError status.
 *  @see                TAPFirmwareOTAUpdateServiceOTAStatus
 */
- (void)system:(id)system checkOTAStatus:(void(^)(TAPFirmwareOTAUpdateServiceOTAStatus, NSString*, NSError*))complete;

/*!
 *  @method system:startOTAUpdateToVersion:usingData:
 *
 *  @param system           The target system
 *  @param version          The OTA version, numbers should be separated by dot, e.g. @"1.0.0"
 *  @param OTAData          The data of the OTA file
 *
 *  @brief                  Start/resume OTA update
 *  @discussion             Calling this method will first check OTA status of system, then a) Start OTA from beginning if current OTA status is TAPFirmwareOTAUpdateServiceOTAStatusNotStarted/ TAPFirmwareOTAUpdateServiceOTAStatusError; b) Start OTA from beginning if target system has been in OTA update and version is not matched; c) Resume OTA update from the downloading offset if target system is TAPFirmwareOTAUpdateServiceOTAStatusDownloading.
 */
- (void)system:(id)system startOTAUpdateToVersion:(NSString*)version usingData:(NSData*)OTAData;

/*!
 *  @method pauseOTAUpdateOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief                  Pause OTA update by stop sending OTA data
 *  @discussion             OTA Service will stop sending OTA data to system and system will be paused in OTA update. This method is valid only when OTA status is before TAPFirmwareOTAUpdateServiceOTAStatusValidated. After OTA status update to TAPFirmwareOTAUpdateServiceOTAStatusValidated, system will kick off OTA update and  nolonger be able to be suspended.
 *  @see    system:startOTAUpdateToVersion:usingData:
 */
- (void)pauseOTAUpdateOfSystem:(id)system;

/*!
 *  @method startMonitorOTAStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the OTA status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE OTA charateristic provided by the system in audio service. Whenever there's a OTA status update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPFirmwareOverTheAirUpdateService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPFirmwareOverTheAirUpdateService](#) , [TAPFirmwareOverTheAirUpdateServiceDelegate system:didUpdateOTAStatus:message:error:] will be triggered and provide the response data.
 *
 *  @see [TAPFirmwareOverTheAirUpdateServiceDelegate system:didUpdateOTAStatus:progress:error:]
 */
- (void)startMonitorOTAStatusOfSystem:(id)system;

/*!
 *  @method stopMonitorOTAStatusOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the OTA status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE OTA charateristic provided by the system in audio service.
 *  @see  startMonitorOTAStatusOfSystem:
 */
- (void)stopMonitorOTAStatusOfSystem:(id)system;

@end

/*!
 *  @protocol TAPFirmwareOverTheAirUpdateServiceDelegate
 *
 *  @brief    Delegate define methods for receive notifications from TAPFirmwareOverTheAirUpdateService.h object.
 *
 */
@protocol TAPFirmwareOverTheAirUpdateServiceDelegate <NSObject>

@required

@optional

/*!
 *  @method system:didUpdateOTAStatus:progress:error:
 *
 *  @param system       The system which has this update
 *  @param status       The new OTA status
 *  @param progress     The new OTA progress
 *  @param error        The OTA error
 *
 *  @brief              Invoked when a the system has a OTA update
 *  @discussion Progress is not valid in TAPFirmwareOTAUpdateServiceOTAStatusNotStarted and TAPFirmwareOTAUpdateServiceOTAStatusError; error is not valid in all status except TAPFirmwareOTAUpdateServiceOTAStatusError.
 *  @see                TAPFirmwareOTAUpdateServiceOTAStatus
 */
- (void)system:(id)system didUpdateOTAStatus:(TAPFirmwareOTAUpdateServiceOTAStatus)status progress:(float)progress error:(NSError*)error;

@end
