//
//  TAPSystem.h
//  TAPProtocol
//
//  Created by Lam Yick Hong on 29/6/15.
//  Copyright (c) 2015 Tymphany. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TAPSystemState){
    /*!
     *  The states where the system is playing audio
     */
    TAPSystemStateDisconnected = 0,
    /*!
     *  The states where the system is disconnected
     */
    TAPSystemStateConnecting,
    
    /*!
     *  The states where the system is connecting
     */
    TAPSystemStateConnected,
    /*!
     *  The states where the system is connected
     */
    TAPSystemStateDisconnecting,
    /*!
     *  The states where the system is unknown state
     */
    TAPSystemStateUnknown
};

FOUNDATION_EXPORT NSString * const TAPSystemMetadataKeyModelId;
FOUNDATION_EXPORT NSString * const TAPSystemMetadataKeyModelAppearanceColor;
FOUNDATION_EXPORT NSString * const TAPSystemMetadataKeyModelServiceData;

@class TAPService;

/*!
 *
 *  @interface TAPSystem
 *  @brief     System to be used in TAP service
 *  @author    Hong Lam
 *  @date      29/1/15
 *  @copyright Tymphany Ltd.
 *
 *  @discussion [TAPSystem](#) is an abstract class of the peripheral class in BLE, WiFi or other communication ways. When [TAPSystem](#) is sent to TAP service, TAP will classifies [TAPSystem](#) and handle the service in right way
 *
 *  This class provided methods to initial, to instance and to obtain the system information
 *
 *  The class member within this class might have different meaning when it's connected to different system. These meaning will be documented on project based docuementation. Please read their description carefully to undertand the meaning and refer to project documentation.
 *
 */
@interface TAPSystem : NSObject

/*!
 * @property name
 *
 *  @discussion
 *      The name of TAPSystem.
 *
 */
@property (copy) NSString *name;

/*!
 * @property UUIDString
 *
 *  @discussion
 * The string description of the identifier of the TAPSystem. Note that this is the property represent a specific information, in string format, guarantee the app to identify a system from another system. To retrieve the exact information you need, you should use other properties.
 *
*/
@property (readonly) NSString *UUIDString;

/*!
 * @property systemIdentifier
 *
 *  @discussion
 *      The string description of the UUID of TAPSystem. This is provided by the operating system.
 *
 */
@property (readonly) NSString *systemIdentifier;

/*!
 * @property macAddress
 *
 *  @discussion
 *      The macAddress of TAPSystem. This property currently represents the macAddress resolved from advertisement. If the macAddres is not available from the advertisment, this property return nil.
 *
 */
@property (copy) NSData *macAddress;

/*!
 * @property state
 *
 *  @discussion
 *      The connection state of TAPSystem. Check the status and ensure that the system is in a state of disconnection before it is connected.
 *
 */
@property (readonly) TAPSystemState state;

/*!
 * @property metadata
 *
 *  @discussion
 *      The metadata that describes this system
 *
 */
@property (readonly) NSMutableDictionary* metadata;

/*!
 *  @method initWithSystem:
 *
 *  @param system   The target system
 *
 *  @brief Initial system
 *
 */
- (id)initWithSystem:(id)system;

/*!
 *  @method instance
 *
 *  @brief Return target peripheral
 *
 *  @discussion Return a peripheral instance
 */
- (id)instance;

/*!
 *  @method updateSystem:
 *
 *  @param system   The new system we would use to replace the old one.
 *
 *  @brief Update the existing system wrapped by our class
 *
 */
- (void)updateSystem:(id)system;

/*!
 *  @method changeDataModeByData:
 *
 *  @param data   The data input that will update the class member of the TAPSystem
 *
 *  @discussion The TAPSystem will interpret the input data and update different class member accordinly. This method is very specific designed for customization requirement. Please be aware whenever you update this method and application developer should only use it when it's very cleared on the behavior and the internal data interpretation.
*/
- (void)changeDataModeByData:(NSData*)data;

@end
