//
//  TAPPlayControlService.h
//  TAPPlayControlService
//
//  Created by Lam Yick Hong on 23/4/15.
//  Copyright (c) 2015 Tymphany. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAPService.h"

/*!
 *  The attribute keys for track info
 */
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoTitle;
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoArtist;
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoAlbum;
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoNumber;
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoTotalNumber;
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoGenre;
FOUNDATION_EXPORT NSString * const TAPPlayControlKeyTrackInfoPlayingTime;

/*!
 *  Values representing the current play status
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServicePlayStatus){
    /*!
     *  The states where the system is playing audio
     */
    TAPPlayControlServicePlayStatusPlay = 0,
    /*!
     *  The states where the system is audio paused
     */
    TAPPlayControlServicePlayStatusPause,
    /*!
     *  The states where the system is audio stopped
     */
    TAPPlayControlServicePlayStatusStopped,
    /*!
     *  The states where the system is in unknown play status
     */
    TAPPlayControlServicePlayStatusUnknown
};

/*!
 *  Values representing the current audio source
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceAudioSource){
    /*!
     *  where the system is in none audio source
     */
    TAPPlayControlServiceAudioSourceNoSource = 0,
    /*!
     *  where the system is in Aux source
     */
    TAPPlayControlServiceAudioSourceAuxIn,
    /*!
     *  where the system is in USB source
     */
    TAPPlayControlServiceAudioSourceUSB,
    /*!
     *  where the system is in Bluetooth
     */
    TAPPlayControlServiceAudioSourceBluetooth,
    /*!
     *  where the system is in RCA source
     */
    TAPPlayControlServiceAudioSourceRCA,
    /*!
     *  where the system is in unknown audio source
     */
    TAPPlayControlServiceAudioSourceUnknown
};

/*!
 *  Values representing the current ANC status
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceANC){
    /*!
     *  where the system is on ANC
     */
    TAPPlayControlServiceANCOff = 0,
    /*!
     *  where the system is off ANC
     */
    TAPPlayControlServiceANCOn,
    /*!
     *  where the system is in unknown ANC status
     */
    TAPPlayControlServiceANCUnknown
};

/*!
 *  Values representing the current ANC mode
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceANCMode){
    /*!
     *  where the system ANC mode is on PBO (play back only)
     */
    TAPPlayControlServiceANCModePBO = 0,
    /*!
     *  where the system ANC mode is on ANC
     */
    TAPPlayControlServiceANCModeANC,
    /*!
     *  where the system ANC mode is on MONITORING
     */
    TAPPlayControlServiceANCModeMonitor,
    /*!
     *  where the system ANC mode is unkown
     */
    TAPPlayControlServiceANCModeUnknown,
};

/*!
 *  Values representing the cue status
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceCue){
    /*!
     *  The target cue of the system is off
     */
    TAPPlayControlServiceCueOff = 0,
    /*!
     *  The target cue of the system is on
     */
    TAPPlayControlServiceCueOn,
    /*!
     *  The target cue of the system is unknown
     */
    TAPPlayControlServiceCueUnknown
};

/*!
 *  Values representing the current TrueWireless status
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceTrueWirelessStatus){
    /*!
     *  where the system is disconnected TrueWireless
     */
    TAPPlayControlServiceTrueWirelessStatusDisconnected = 0,
    /*!
     *  where the system is reconnecting TrueWireless
     */
    TAPPlayControlServiceTrueWirelessStatusReconnecting,
    /*!
     *  where the system is pairing master
     */
    TAPPlayControlServiceTrueWirelessStatusMasterPairing,
    /*!
     *  where the system is pairing slave
     */
    TAPPlayControlServiceTrueWirelessStatusSlavePairing,
    /*!
     *  where the system is connected as master
     */
    TAPPlayControlServiceTrueWirelessStatusConnectedAsMaster,
    /*!
     *  where the system is connected as slave
     */
    TAPPlayControlServiceTrueWirelessStatusConnectedAsSlave,
    /*!
     *  where the system is pairing master mac address
     */
    TAPPlayControlServiceTrueWirelessStatusMasterMACAddressPairing,
    /*!
     *  where the system is in unknown TrueWireless status
     */
    TAPPlayControlServiceTrueWirelessStatusUnknown
};

/*!
 *  Values representing the current TrueWireless channel setting
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceTrueWirelessChannel){
    /*!
     *  where the system set party channel
     */
    TAPPlayControlServiceTrueWirelessChannelParty = 0,
    /*!
     *  where the system set master as left
     */
    TAPPlayControlServiceTrueWirelessChannelMasterAsLeft,
    /*!
     *  where the system set master as right
     */
    TAPPlayControlServiceTrueWirelessChannelMasterAsRight,
    /*!
     *  where the system is in unknown TrueWireless channel setting
     */
    TAPPlayControlServiceTrueWirelessChannelUnknown
};

/*!
 *  Values representing the current Share Me status
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceShareMeStatus){
    /*!
     *  where the system Share Me is disconnected
     */
    TAPPlayControlServiceShareMeStatusDisconnected = 0,
    /*!
     *  where the system Share Me is connected as master
     */
    TAPPlayControlServiceShareMeStatusConnectedAsMaster,
    /*!
     *  where the system Share Me is connected as slave
     */
    TAPPlayControlServiceShareMeStatusConnectedAsSlave,
    /*!
     *  where the system Share Me is in master pairing mode
     */
    TAPPlayControlServiceShareMeStatusMasterPairing,
    /*!
     *  where the system Share Me is in slave pairing mode
     */
    TAPPlayControlServiceShareMeStatusSlavePairing,
    /*!
     *  where the system Share Me is in unknown status
     */
    TAPPlayControlServiceShareMeStatusUnknown
};

/*!
 *  Values representing the current play status
 */
typedef NS_ENUM(NSInteger, TAPPlayControlServiceCustomButtonMode){
    /*!
     *  The configuration means the button is set to EQ controls(default)
     */
    TAPPlayControlServiceCustomButtonModeEQControl = 5,
    /*!
     *  The configuration means the button is set to Google Voice Assistant
     */
    TAPPlayControlServiceCustomButtonModeGoogleVoiceAssistant = 6,
    /*!
     *  The configuration means the button is set to Alexa
     */
    TAPPlayControlServiceCustomButtonModeAlexa = 7,
    /*!
     *  The configuration means the button is set to native voice assistant(e.g. Siri)
     */
    TAPPlayControlServiceCustomButtonModeNativeVoiceAssistant = 3,
    /*!
     *  The configuration means the button is in unknown state
     */
    TAPPlayControlServiceCustomButtonModeUnknown = 8
};

@protocol TAPPlayControlServiceDelegate;

/*!
 *
 *  @interface TAPPlayControlService
 *  @brief     TAP Service for controlling the audio play back. Any basic playback control will be provided here.
 *  @author    Hong Lam
 *  @date      23/4/15
 *  @copyright Tymphany Ltd.
 *
 *  @discussion     TAP Service for controlling the audio play back. Any basic playback control will be provided here.
 *
 *
 *  [TAPPlayControlService](#) objects are used to control the audio playback on the systems, including play, pause, next track, previous track, volume control and current playback info.
 *
 *  You should implement the [TAPPlayControlServiceDelegate](#) and assign to the [TAPPlayControlService](#) object to receive any status update of the audio playback.
 *
 */
@interface TAPPlayControlService : TAPService

/*!
 *  @property   delegate
 *  @brief      A TAPPlayControlService delegate object
 *  @see        TAPPlayControlServiceDelegate
 */
@property (nonatomic, strong) id<TAPPlayControlServiceDelegate> delegate;

/*!
 *  @method system:volume:
 *
 *  @param system           The target system
 *  @param volume           The block which taking the volume as a parameter
 *
 *  @brief                  Reads the volume from the target system
 *
 *  @discussion On BLE, the complete block will return an integer object which presenting the currnet volume in text form.
 */
- (void)system:(id)system volume:(void (^)(NSInteger))volume;

/*!
 *  @method system:writeVolume:completion:
 *
 *  @param system           The target system
 *  @param volume           The target volume, range in [0,127]
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the volume to the target system
 *
 *  @discussion On BLE, the complete block will return the BLE response. An NSError object otherwise.
 */
- (void)system:(id)system writeVolume:(NSInteger)volume completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method volumeUpSystem:completion:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Up volume by one step to the target system
 *
 *  @discussion On BLE, the complete block will return the BLE response. An NSError object otherwise.
 */
- (void)volumeUpSystem:(id)system completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method volumeDownSystem:completion:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Up volume by one step to the target system
 *
 *  @discussion On BLE, the complete block will return the BLE response. An NSError object otherwise.
 */
- (void)volumeDownSystem:(id)system completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorVolumeOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the volume of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE volume charateristic provided by the system in audio service. Whenever there's a volume update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateVolume:] will be triggered and provide the response data.
 *
 *  @see [TAPPlayControlServiceDelegate system:didUpdateVolume:]
 */
- (void)startMonitorVolumeOfSystem:(id)system;

/*!
 *  @method stopMonitorVolumeOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the volume of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE volume charateristic provided by the system in audio service.
 *  @see  startMonitorVolumeOfSystem:
 */
- (void)stopMonitorVolumeOfSystem:(id)system;

/*!
 *  @method system:audioSource:
 *
 *  @param system           The target system
 *  @param source           The block which taking the audioSource as a parameter
 *
 *  @brief                  Read the current audio source from the target system
 *  @see                    TAPPlayControlServiceAudioSource
 *
 */
- (void)system:(id)system audioSource:(void (^)(TAPPlayControlServiceAudioSource))source;

/*!
 *  @method system:BluetoothSource:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Switch audio source to bluetooth of the target system
 *
 *
 */
- (void)system:(id)system BluetoothSource:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:AuxSource:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Switch audio source to Aux of the target system
 *
 *
 */
- (void)system:(id)system AuxSource:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:RCASource:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Switch audio source to RCA of the target system
 *
 *
 */
- (void)system:(id)system RCASource:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:USBSource:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Switch audio source to USB of the target system
 *
 *
 */
- (void)system:(id)system USBSource:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:playStatus:
 *
 *  @param system           The target system
 *  @param status           The block which taking the status as a parameter
 *
 *  @brief                  Read the status from the target system
 *  @see                    TAPPlayControlServicePlayStatus
 *
 */
- (void)system:(id)system playStatus:(void (^)(TAPPlayControlServicePlayStatus))status;

/*!
 *  @method system:play:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Plays audio of the target system
 *
 *
 */
- (void)system:(id)system play:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:pause:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Pauses audio of the target system
 *
 *
 */
- (void)system:(id)system pause:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:next:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Skips to next track
 *
 *
 */
- (void)system:(id)system next:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:previous:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Skips to previous track
 *
 *
 */
- (void)system:(id)system previous:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:anc:
 *
 *  @param system           The target system
 *  @param anc              The block which taking the anc as a parameter
 *
 *  @brief                  Read the ANC from the target system
 *  @see                    TAPPlayControlServiceANC
 *
 */
- (void)system:(id)system anc:(void (^)(TAPPlayControlServiceANC))anc;

/*!
 *  @method system:turnANC:completion:
 *
 *  @param system           The target system
 *  @param anc              The ANC status
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Trun ANC status of the target system
 *  @see                    TAPPlayControlServiceANC
 *
 */
- (void)system:(id)system turnANC:(TAPPlayControlServiceANC)anc completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:ancMode:
 *
 *  @param system           The target system
 *  @param ancMode          The block which taking the anc mode as a parameter
 *
 *  @brief                  Read current anc mode from the target system
 *  @see                    TAPPlayControlServiceANCMode
 *
 */
- (void)system:(id)system ancMode:(void (^)(TAPPlayControlServiceANCMode))ancMode;

/*!
 *  @method system:turnToANCMode:completion:
 *
 *  @param system           The target system
 *  @param ancMode          Target anc mode
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Turn anc mode of the target system
 *  @see                    TAPPlayControlServiceANCMode
 *
 */
- (void)system:(id)system turnToANCMode:(TAPPlayControlServiceANCMode)ancMode completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:ancLevel:
 *
 *  @param system           The target system
 *  @param level            The level which taking the anc level as a parameter, the range of level is defined by firmware
 *
 *  @brief                  Read the ANC level from the target system
 *
 */
- (void)system:(id)system ancLevel:(void (^)(NSInteger))level;

/*!
 *  @method system:setANCLevel:completion:
 *
 *  @param system           The target system
 *  @param level            Target anc level, should not exceed 255
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Set anc level of the target system
 *
 */
- (void)system:(id)system setANCLevel:(NSInteger)level completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:monitorLevel:
 *
 *  @param system           The target system
 *  @param level            The level which taking the monitor level as a parameter, the range of level is defined by firmware
 *
 *  @brief                  Read the monitor level from the target system
 *
 */
- (void)system:(id)system monitorLevel:(void (^)(NSInteger))level;

/*!
 *  @method system:setMonitorLevel:completion:
 *
 *  @param system           The target system
 *  @param level            Target monitor level, should not exceed 255
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Set monitor level of the target system
 *  @discussion
 */
- (void)system:(id)system setMonitorLevel:(NSInteger)level completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorAncModeOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the anc mode of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE ANC charateristic provided by the system in audio service. Whenever there's an anc mode update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateANCMode:] will be triggered and provide the response data.
 *
 *  @see [TAPPlayControlServiceDelegate system:didUpdateANCMode:]
 */
- (void)startMonitorAncModeOfSystem:(id)system;

/*!
 *  @method stopMonitorAncModeOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the anc mode of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE anc charateristic provided by the system in audio service.
 *  @see  startMonitorAncModeOfSystem:
 */
- (void)stopMonitorAncModeOfSystem:(id)system;

/*!
 *  @method system:powerCue:
 *
 *  @param system           The target system
 *  @param powerCue         The power cue status
 *
 *  @brief                  Read the power cue status of the target system
 *  @see                    TAPPlayControlServiceCue
 *
 */
- (void)system:(id)system powerCue:(void (^)(TAPPlayControlServiceCue))powerCue;

/*!
 *  @method system:turnPowerCue:completion:
 *
 *  @param system           The target system
 *  @param cue              The power cue status
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Trun power cue status of the target system
 *  @see                    TAPPlayControlServiceCue
 *
 */
- (void)system:(id)system turnPowerCue:(TAPPlayControlServiceCue)cue completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:mediaCue:
 *
 *  @param system           The target system
 *  @param mediaCue         The media cue status
 *
 *  @brief                  Read the media cue status of the target system
 *  @see                    TAPPlayControlServiceCue
 *
 */
- (void)system:(id)system mediaCue:(void (^)(TAPPlayControlServiceCue))mediaCue;

/*!
 *  @method system:turnMediaCue:completion:
 *
 *  @param system           The target system
 *  @param cue              The media cue status
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Trun media cue status of the target system
 *  @see                    TAPPlayControlServiceCue
 *
 */
- (void)system:(id)system turnMediaCue:(TAPPlayControlServiceCue)cue completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:ancCue:
 *
 *  @param system           The target system
 *  @param ancCue         The ANC cue status
 *
 *  @brief                  Read the ANC cue status of the target system
 *  @see                    TAPPlayControlServiceCue
 *
 */
- (void)system:(id)system ancCue:(void (^)(TAPPlayControlServiceCue))ancCue;

/*!
*  @method system:turnANCCue:completion:
*
*  @param system           The target system
*  @param cue              The ANC cue status
*  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
*
*  @brief                  Trun ANC cue status of the target system
*  @see                    TAPPlayControlServiceCue
*
*/
- (void)system:(id)system turnANCCue:(TAPPlayControlServiceCue)cue completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:configButtonCue:
 *
 *  @param system           The target system
 *  @param configButtonCue         The ConfigButton cue status
 *
 *  @brief                  Read the ConfigButton cue status of the target system
 *  @see                    TAPPlayControlServiceCue
 *
 */
- (void)system:(id)system configButtonCue:(void (^)(TAPPlayControlServiceCue))configButtonCue;

/*!
*  @method system:turnConfigButtonCue:completion:
*
*  @param system           The target system
*  @param cue              The Config Button cue status
*  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
*
*  @brief                  Turn Config Button cue status of the target system
*  @see                    TAPPlayControlServiceCue
*
*/
- (void)system:(id)system turnConfigButtonCue:(TAPPlayControlServiceCue)cue completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorAudioControlAndStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the audio control status and play status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE audio control and status charateristic provided by the system in audio service. Whenever there's a audio control and status change, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateAudioSource:] [TAPPlayControlServiceDelegate system:didUpdatePlayStatus:] [TAPPlayControlServiceDelegate system:didUpdateANC:] [TAPPlayControlServiceDelegate system:didUpdatePowerCue:] and [TAPPlayControlServiceDelegate system:didUpdateMediaCue:] will be triggered and provide the response data.
 *
 *  @see [TAPPlayControlServiceDelegate system:didUpdateAudioSource:]
 *  @see [TAPPlayControlServiceDelegate system:didUpdatePlayStatus:]
 *  @see [TAPPlayControlServiceDelegate system:didUpdateANC:]
 *  @see [TAPPlayControlServiceDelegate system:didUpdatePowerCue:]
 *  @see [TAPPlayControlServiceDelegate system:didUpdateMediaCue:]
 */
- (void)startMonitorAudioControlAndStatusOfSystem:(id)system;

/*!
 *  @method stopMonitorAudioControlAndStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the audio control status and play status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE audio control and status charateristic provided by the system in audio service.
 *  @see startMonitorAudioControlAndStatusOfSystem:
 */
- (void)stopMonitorAudioControlAndStatusOfSystem:(id)system;

/*!
 *  @method system:trueWirelessConfig:
 *
 *  @param system           The target system
 *  @param config           The block which taking the TrueWireless status, channel setting and TrueWireless paired device MAC address as a parameter
 *
 *  @brief                  Read the TrueWireless status of the target system
 *
 *  @discussion             On BLE common mode, calling this method will trigger the App read to the BLE TrueWireless status charateristic provided by the system in audio service.
 *
 *  Once read succeeded, you will get 3 parameters in status block, they are TrueWireless status, TrueWireless channel and TrueWireless paired device MAC address.
 *
 *  TrueWireless status is always valid, see description of TAPPlayControlServiceTrueWirelessStatus for different status;
 *
 *   TrueWireless channel is only valid when TrueWireless status is TAPPlayControlServiceTrueWirelessStatusConnectedAsMaster or TAPPlayControlServiceTrueWirelessStatusConnectedAsSlave;
 *
 *   TrueWireless paired device MAC address is only valid when TrueWireless status is TAPPlayControlServiceTrueWirelessStatusConnectedAsMaster or TAPPlayControlServiceTrueWirelessStatusConnectedAsSlave. For example, when target system is connected as master, you will get the MAC address of the slave and the reverse is the same. You can use this MAC address to identify the TrueWireless paired device.
 *
 *  @warning                In earlier firmware version, TrueWireless paired device MAC address is not supported and the return MAC address will be nil.
 *
 *  @see                    TAPPlayControlServiceTrueWirelessStatus
 *  @see                    TAPPlayControlServiceTrueWirelessChannel
 */
- (void)system:(id)system trueWirelessConfig:(void (^)(TAPPlayControlServiceTrueWirelessStatus, TAPPlayControlServiceTrueWirelessChannel, NSData*))config;

/*!
 *  @method system:disconnectTrueWireless:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the TrueWireless disconnect command to system
 *
 *
 */
- (void)system:(id)system disconnectTrueWireless:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:pairingTrueWirelessToMaster:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the TrueWireless master pairing command to system
 *
 *
 */
- (void)system:(id)system pairingTrueWirelessToMaster:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:pairingTrueWirelessToSlave:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the TrueWireless slave pairing command to system
 *
 *
 */
- (void)system:(id)system pairingTrueWirelessToSlave:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:pairingTrueWirelessToMACAddress:completion:
 *
 *  @param system           The target system
 *  @param address          The target MAC address
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the TrueWireless MAC address pairing command to system
 *  @discussion             iOS hides the MAC address of device and generates a UUID instead, which means iOS App could not get the MAC address of discovered device directly. In this case, we use a custom characteristic to read the MAC address of connected device by Firmware. You can get the MAC address of connected device by [TAPSystemService system:MACAddress:].
 *
 *  @see                    [TAPSystemService system:MACAddress:]
 */
- (void)system:(id)system pairingTrueWirelessToMACAddress:(NSData*)address completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:switchTrueWirelessChannel:completion:
 *
 *  @param system           The target system
 *  @param channel          The target TrueWireless channel
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write the TrueWireless channel switch command to system
 *  @see                    TAPPlayControlServiceTrueWirelessChannel
 *
 */
- (void)system:(id)system switchTrueWirelessChannel:(TAPPlayControlServiceTrueWirelessChannel)channel completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorTrueWirelessConfigOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the TrueWireless Config of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE TrueWireless config charateristic provided by the system in audio service. Whenever there's a TrueWireless config change, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateTrueWirelessConfig:] will be triggered and provide the response data.
 *
 *  @see [TAPPlayControlServiceDelegate system:didUpdateTrueWirelessConfig:]
 */
- (void)startMonitorTrueWirelessConfigOfSystem:(id)system;

/*!
 *  @method stopMonitorTrueWirelessConfigOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the TrueWireless of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE TrueWireless config charateristic provided by the system in audio service.
 *  @see  startMonitorTrueWirelessConfigOfSystem:
 */
- (void)stopMonitorTrueWirelessConfigOfSystem:(id)system;

/*!
 *  @method system:currentTrackInfo:
 *
 *  @param system           The target system
 *  @param trackInfo        The block which taking the current track info as a parameter
 *
 *  @brief                  Retreives the current track info
 *
 *  @discussion Use attribute keys to extract value of the track info: key TAPPlayControlKeyTrackInfoTitle for name of the artist; key TAPPlayControlKeyTrackInfoArtist for name of the album, TAPPlayControlKeyTrackInfoAlbum; key TAPPlayControlKeyTrackInfoNumber for number of the media(e.g. track number of the CD); key  TAPPlayControlKeyTrackInfoTotalNumber for total number of the media(e.g. total track number of the CD);  key TAPPlayControlKeyTrackInfoGenre for genre; key  TAPPlayControlKeyTrackInfoPlayingTime for time in milliseconds
 */
- (void)system:(id)system currentTrackInfo:(void (^)(NSDictionary*))trackInfo;

/*!
 *  @method startMonitorCurrentTrackInfoOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the current track info of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE current track info charateristic provided by the system in audio service. Whenever there's a status update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateCurrentTrackInfo:] will be triggered and provide the response data.
 *
 *  @see [TAPPlayControlServiceDelegate system:didUpdateCurrentTrackInfo:]
 */
- (void)startMonitorCurrentTrackInfoOfSystem:(id)system;

/*!
 *  @method stopMonitorCurrentTrackInfoOfSystem:
 *
 *  @param system           The target system
 *
 *  @brief  Stops monitoring the current track info of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscribe to the BLE track info charateristic provided by the system in audio service.
 *  @see  startMonitorCurrentTrackInfoOfSystem:
 */
- (void)stopMonitorCurrentTrackInfoOfSystem:(id)system;

/*!
 *  @method system:shareMeStatus:
 *
 *  @param system           The target system
 *  @param status           The block which taking the share me status and paired device MAC address as a parameter
 *
 *  @brief                  Read Share Me status of target system
 *
 *  @discussion             On BLE common mode, calling this method will trigger the App read to the BLE share me status charateristic provided by the system in audio service.
 *
 *  Once read succeeded, you will get 2 parameters in status block, they are share me status and paired device MAC address.
 *
 *  Share me status is always valid, see description of TAPPlayControlServiceShareMeStatus for different status;
 *
 *  Paired device MAC address only vaild when share me status is connected as master, otherwise it will be nil. The MAC address can be used to identify the slave device.
 *
 *  @see                    TAPPlayControlServiceShareMeStatus
 */
- (void)system:(id)system shareMeStatus:(void (^)(TAPPlayControlServiceShareMeStatus, NSData*))status;

/*!
 *  @method system:disconnectShareMe:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Disconnect or cancel pairing Share Me to system
 *
 */
- (void)system:(id)system disconnectShareMe:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:pairingShareMeToMACAddress:completion:
 *
 *  @param system           The target system
 *  @param address          The MAC address of target device
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Pairing Share Me to target device with its MAC address
 *  @discussion             iOS hides the MAC address of device and generates a UUID instead, which means iOS App could not get the MAC address of discovered device directly. In this case, we use a custom characteristic to read the MAC address of connected device by Firmware. You can get the MAC address of connected device by [TAPSystemService system:MACAddress:].
 *
 *  @see                    [TAPSystemService system:MACAddress:]
 */
- (void)system:(id)system pairingShareMeToMACAddress:(NSData*)address completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:triggerShareMeMasterPairing:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Trigger device to Share Me master pairing mode, so that device can establish Share Me connection
 *
 */
- (void)system:(id)system triggerShareMeMasterPairing:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:triggerShareMeSlavePairing:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Trigger device to Share Me slave pairing mode, so that device can establish Share Me connection
 *
 */
- (void)system:(id)system triggerShareMeSlavePairing:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorShareMeOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring Share Me status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE Share Me charateristic provided by the system in audio service. Whenever there's a Share Me status update, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateShareMeStatus:] will be triggered and provide the response data.
 *
 *  @see [TAPPlayControlServiceDelegate system:didUpdateShareMeStatus:]
 */
- (void)startMonitorShareMeOfSystem:(id)system;

/*!
 *  @method stopMonitorShareMeOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring Share Me status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE Share Me charateristic provided by the system in audio service.
 *  @see  startMonitorShareMeOfSystem:
 */
- (void)stopMonitorShareMeOfSystem:(id)system;

/*!
 *  @method system:customButtonMode:
 *
 *  @param system           The target system
 *  @param mode             The block which taking the custom button mode as a parameter
 *
 *  @brief                  Read custom button mode of target system
 *
 *  @see                    TAPPlayControlServiceCustomButtonMode
 */
- (void)system:(id)system customButtonMode:(void (^)(TAPPlayControlServiceCustomButtonMode))mode;

/*!
 *  @method system:writeFunctionConfiguration:completion:
 *
 *  @param system           The target system
 *  @param featureID           The target featureID, range in [0,255]
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the value represent a custom function to the target system
 *
 *  @discussion On BLE, the complete block will return the BLE response. An NSError object otherwise.
 */
- (void)system:(id)system writeFunctionConfiguration:(NSInteger)featureID completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:switchToCustomButtonMode:completion:
 *
 *  @param system           The target system
 *  @param mode             The custom button mode
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Switch custom button mode of the target system
 *  @see                    TAPPlayControlServiceCustomButtonMode
 *
 */
- (void)system:(id)system switchToCustomButtonMode:(TAPPlayControlServiceCustomButtonMode)mode completion:(void (^)(NSDictionary*))complete;

/*!
*  @method startMonitorCustomButtonModeOfSystem:
*
*  @param system   The target system
*
*  @brief  Starts monitoring the custom button mode of the target system.
*
*  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE Function Configuration charateristic provided by the system. Whenever there's a custom function configuration, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPPlayControlService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPPlayControlService](#) , [TAPPlayControlServiceDelegate system:didUpdateCustomButtonMode:] will be triggered and provide the response data.
*
*  @see [TAPPlayControlServiceDelegate system:didUpdateCustomButtonMode:]
*/
- (void)startMonitorCustomButtonModeOfSystem:(id)system;

/*!
*  @method stopMonitorCustomButtonModeOfSystem:
*
*  @param system   The target system
*
*  @brief  Stops monitoring custom button mode of the target system.
*
*  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE function configuration charateristic provided by the system in play control service.
*  @see  startMonitorCustomButtonModeOfSystem:
*/
- (void)stopMonitorCustomButtonModeOfSystem:(id)system;

@end

/*!
 *  @protocol TAPPlayControlServiceDelegate
 *
 *  @brief    Delegate define methods for receive notifications from TAPPlayControlService object.
 *
 */
@protocol TAPPlayControlServiceDelegate <NSObject>

@required

@optional

/*!
 *  @method system:didUpdateVolume:
 *
 *  @param system       The system which has this update
 *  @param volume       The new volume value
 *
 *  @brief              Invoked when a the system has a volume update
 */
- (void)system:(id)system didUpdateVolume:(NSInteger)volume;

/*!
 *  @method system:didUpdateAudioSource:
 *
 *  @param system       The system which has this update
 *  @param audioSource  The new audio source
 *
 *  @brief              Invoked when a the system has a audio source update
 *  @see                TAPPlayControlServiceAudioSource
 */
- (void)system:(id)system didUpdateAudioSource:(TAPPlayControlServiceAudioSource)audioSource;

/*!
 *  @method system:didUpdatePlayStatus:
 *
 *  @param system       The system which has this update
 *  @param playStatus   The new play status
 *
 *  @brief              Invoked when a the system has a play status update
 *  @see                TAPPlayControlServicePlayStatus
 */
- (void)system:(id)system didUpdatePlayStatus:(TAPPlayControlServicePlayStatus)playStatus;

/*!
 *  @method system:didUpdateANC:
 *
 *  @param system       The system which has this update
 *  @param ANC          The new ANC status
 *
 *  @brief              Invoked when a the system has a ANC update
 *  @see                TAPPlayControlServiceANC
 */
- (void)system:(id)system didUpdateANC:(TAPPlayControlServiceANC)ANC;

/*!
 *  @method system:didUpdatePowerCue:
 *
 *  @param system       The system which has this update
 *  @param powerCue     The new power cue status
 *
 *  @brief              Invoked when a the system has a power cue update
 *  @see                TAPPlayControlServiceCue
 */
- (void)system:(id)system didUpdatePowerCue:(TAPPlayControlServiceCue)powerCue;

/*!
 *  @method system:didUpdateMediaCue:
 *
 *  @param system       The system which has this update
 *  @param mediaCue     The new media cue status
 *
 *  @brief              Invoked when a the system has a media cue update
 *  @see                TAPPlayControlServiceCue
 */
- (void)system:(id)system didUpdateMediaCue:(TAPPlayControlServiceCue)mediaCue;

/*!
*  @method system:didUpdateANCCue:
*
*  @param system       The system which has this update
*  @param ancCue     The new ANC cue status
*
*  @brief              Invoked when a the system has a ANC cue update
*  @see                TAPPlayControlServiceCue
*/
- (void)system:(id)system didUpdateANCCue:(TAPPlayControlServiceCue)ancCue;

/*!
*  @method system:didUpdateConfigButtonCue:
*
*  @param system       The system which has this update
*  @param configButtonCue     The new Config Button cue status
*
*  @brief              Invoked when a the system has a Config Button cue update
*  @see                TAPPlayControlServiceCue
*/
- (void)system:(id)system didUpdateConfigButtonCue:(TAPPlayControlServiceCue)configButtonCue;

/*!
 *  @method system:didUpdateCurrentTrackInfo:
 *
 *  @param system       The system which has this update
 *  @param trackInfo    The new track info
 *
 *  @brief              Invoked when a the system has a track info update
 *
 *  @discussion Use attribute keys to extract string value of the track info: key TAPPlayControlKeyTrackInfoTitle for name of the artist; key TAPPlayControlKeyTrackInfoArtist for name of the album, TAPPlayControlKeyTrackInfoAlbum; key TAPPlayControlKeyTrackInfoNumber for number of the media(e.g. track number of the CD); key  TAPPlayControlKeyTrackInfoTotalNumber for total number of the media(e.g. total track number of the CD);  key TAPPlayControlKeyTrackInfoGenre for genre; key  TAPPlayControlKeyTrackInfoPlayingTime for time in milliseconds
 */
- (void)system:(id)system didUpdateCurrentTrackInfo:(NSDictionary*)trackInfo;

/*!
 *  @method system:didUpdateTrueWirelessStatus:channel:
 *
 *  @param system       The system which has this update
 *  @param status       The new TrueWireless status
 *  @param channel      The new TrueWireless channel
 *
 *  @brief              Invoked when a the system has a TrueWireless update
 *
 *  @warning            This method is deprecated due to TYM BLE profile spec change, use [TAPPlayControlServiceDelegate didUpdateTrueWirelessStatus:channel:TrueWirelessPairedDeviceMACAddress:] instead
 *
 *  @see                [TAPPlayControlServiceDelegate didUpdateTrueWirelessStatus:channel:TrueWirelessPairedDeviceMACAddress:]
 */
- (void)system:(id)system didUpdateTrueWirelessStatus:(TAPPlayControlServiceTrueWirelessStatus)status channel:(TAPPlayControlServiceTrueWirelessChannel)channel __attribute__((deprecated));

/*!
 *  @method system:didUpdateTrueWirelessStatus:channel:TrueWirelessPairedDeviceMACAddress:
 *
 *  @param system       The system which has this update
 *  @param status       The new TrueWireless status
 *  @param channel      The new TrueWireless channel
 *  @param MACAddress   The MAC address of TrueWireless paired device
 *
 *  @brief              Invoked when a the system has a TrueWireless status update
 *  @discussion         TrueWireless status is always valid, see description of TAPPlayControlServiceTrueWirelessStatus for different status;
 *
 *  TrueWireless channel is only valid when TrueWireless status is TAPPlayControlServiceTrueWirelessStatusConnectedAsMaster or TAPPlayControlServiceTrueWirelessStatusConnectedAsSlave;
 *
 *  TrueWireless paired device MAC address is only valid when TrueWireless status is TAPPlayControlServiceTrueWirelessStatusConnectedAsMaster or TAPPlayControlServiceTrueWirelessStatusConnectedAsSlave. For example, when target system is connected as master, you will get the MAC address of the slave and the reverse is the same. You can use this MAC address to identify the TrueWireless paired device.
 *
 *  @warning            In earlier firmware version, TrueWireless paired device MAC address is not supported and the return MAC address will be nil.
 *
 *  @see                TAPPlayControlServiceTrueWirelessStatus
 *  @see                TAPPlayControlServiceTrueWirelessChannel
 */
- (void)system:(id)system didUpdateTrueWirelessStatus:(TAPPlayControlServiceTrueWirelessStatus)status channel:(TAPPlayControlServiceTrueWirelessChannel)channel TrueWirelessPairedDeviceMACAddress:(NSData*)MACAddress;

/*!
 *  @method system:didUpdateShareMeStatus:shareMePairedDeviceMACAddress:
 *
 *  @param system       The system which has this update
 *  @param status       The new Share Me status
 *  @param MACAddress   The new Share Me paired device MAC address
 *
 *  @brief              Invoked when a the system has a Share Me status update
 *
 *  @discussion         Share me status is always valid, see description of TAPPlayControlServiceShareMeStatus for different status;
 *
 *
 *  Paired device MAC address only vaild when share me status is connected as master, otherwise it will be nil. The MAC address can be used to identify the slave device.
 *
 *  @see                TAPPlayControlServiceShareMeStatus
 */
- (void)system:(id)system didUpdateShareMeStatus:(TAPPlayControlServiceShareMeStatus)status shareMePairedDeviceMACAddress:(NSData*)MACAddress;

/*!
 *  @method system:didUpdateCustomButtonMode:
 *
 *  @param system       The system which has this update
 *  @param mode       The new custom button mode
 *
 *  @brief              Invoked when a the system has a custom button mode update
 *
 *  @discussion         See description of TAPPlayControlServiceCustomButtonMode for different mode
 *
 *  @see                TAPPlayControlServiceCustomButtonMode
 */
- (void)system:(id)system didUpdateCustomButtonMode:(TAPPlayControlServiceCustomButtonMode)mode;

/*!
 *  @method system:didUpdateANCMode:
 *
 *  @param system       The system which has this update
 *  @param ancMode      The new anc mode
 *
 *  @brief              Invoked when a the system has anc mode update
 *  @see                TAPPlayControlServiceANCMode
 */
- (void)system:(id)system didUpdateANCMode:(TAPPlayControlServiceANCMode)ancMode;

@end

