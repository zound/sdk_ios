//
//  TAPSystemService.h
//  TAPSystemService
//
//  Created by Lam Yick Hong on 31/3/15.
//  Copyright (c) 2015 Tymphany. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TAPService.h"

FOUNDATION_EXPORT NSString * const TAPSystemKeyDisplay;
FOUNDATION_EXPORT NSString * const TAPSystemKeyTimeout;
FOUNDATION_EXPORT NSString * const TAPSystemKeyStandby;
FOUNDATION_EXPORT NSString * const TAPSystemKeyBrightness;

FOUNDATION_EXPORT NSString * const TAPSystemKeyConnectionDuration;

FOUNDATION_EXPORT NSString * const TAPSystemKeyAdvertisementDataIsConnectable;
FOUNDATION_EXPORT NSString * const TAPSystemKeyAdvertisementDataLocalName;
FOUNDATION_EXPORT NSString * const TAPSystemKeyAdvertisementDataManufacturerData;
FOUNDATION_EXPORT NSString * const TAPSystemKeyAdvertisementDataServiceUUIDs;
FOUNDATION_EXPORT NSString * const TAPSystemKeyAdvertisementDataServiceDataKey;


/*!
 *  Values representing the current power status
 */
typedef NS_ENUM(NSInteger, TAPSystemServicePowerStatus){
    /*!
     *  The states where the system is completely turned off
     */
    TAPSystemServicePowerStatusCompletelyOff = 0,
    /*!
     *  The states where the system is turned on
     */
    TAPSystemServicePowerStatusOn,
    /*!
     *  The states where the system is standby low
     */
    TAPSystemServicePowerStatusStandbyLow,
    /*!
     *  The states where the system is standby high
     */
    TAPSystemServicePowerStatusStandbyHigh,
    /*!
     *  The states where the system is in unknown status
     */
    TAPSystemServicePowerStatusUnknown
};


/*!
 *  Values representing the state of the system service
 */
typedef NS_ENUM(NSInteger, TAPSystemServiceState){
    /*!
     *  The states where the system service is ready, can start scanning
     */
    TAPSystemServiceStateReady = 0,
    /*!
     *  The states where the system service is off
     */
    TAPSystemServiceStateOff,
    /*!
     *  The states where the system service is not supporting the assigned protocol
     */
    TAPSystemServiceStateUnsupported,
    /*!
     *  The states where the system service is not authorized to communicate with the system
     */
    TAPSystemServiceStateUnauthorized,
    /*!
     *  The states where the system service is in unknown
     */
    TAPSystemServiceStateUnknown
};

/*!
 *  Values representing the pairing state of the system
 */
typedef NS_ENUM(NSInteger, TAPSystemServicePairingStatus){
    /*!
     *  The states where the pairing mode is started
     */
    TAPSystemServicePairingStatusStarted = 2,
    /*!
     *  The states where the pairing mode is started
     */
    TAPSystemServicePairingStatusStopped,
    /*!
     *  The states where the pairing mode status is unknown
     */
    TAPSystemServicePairingStatusUnknown
};


/*!
 *  Values representing the connection state of the power cable
 */
typedef NS_ENUM(NSInteger, TAPSystemServicePowerCableConntectionStatus){
    /*!
     *  The states where the power cable is not connected
     */
    TAPSystemServicePowerCableConntectionStatusNotConnected = 0,
    /*!
     *  The states where the power cable is connected
     */
    TAPSystemServicePowerCableConntectionStatusConnected,
    /*!
     *  The states where the power cable status is unknown
     */
    TAPSystemServicePowerCableConntectionStatusUnknown
};

@protocol TAPSystemServiceDelegate;

/*!
 *
 *  @interface TAPSystemService
 *  @brief     TAP Service for managing the system. Helps exchange device/system related information.
 *  @author    Hong Lam
 *  @date      31/3/15
 *  @copyright Tymphany Ltd.
 *
 *  @discussion TAP Service for managing the system. Helps exchange device/system related information.
 *
 *   [TAPSystemService](#) objects are used to retrieve or manipulate any settings on a system with Tymphany system software. This service is particularly important which certain message related to other communication will be returned from this service. E.g. on BLE case, this is the service which notify the App BLE status and if the system is ready for BLE communication. You should not call any API on other service if the [TAPSystemService](#) object have not notify you BLE state is ready.
 *
 *  You should implement the [TAPSystemServiceDelegate](#) and assign to the TAPSystemService object to receive any status update related to the system.
 *
 */
@interface TAPSystemService : TAPService

/*!
 *  @property   delegate
 *  @brief      A TAPSystemService delegate object
 */
@property (nonatomic, strong) id<TAPSystemServiceDelegate> delegate;

/*!
 *  @property   state
 *  @brief      The current state of the system service
 *  @see        TAPSystemServiceState
 */
@property TAPSystemServiceState state;

/*!
 *  @method initWithType:config:
 *
 *  @param type     The type of the protocol.
 *  @param config   The configuration dictionary
 *  @param delegate The system service delegate object
 *
 *  @brief              Initiates a service instance which make use of a protocol object, with a custom config
 *
 */
- (id)initWithType:(NSString*)type config:(NSDictionary*)config delegate:(id<TAPSystemServiceDelegate>)delegate;

/*!
 *  @method connectedSystems
 *
 *  @brief  Return connected systems
 *
 *  @return Return connected systems
 *
 *  @discussion The list of connected peripherals can include those that are connected by other apps
 */
- (NSArray*)connectedSystems;

/*!
 *  @method scanForSystems
 *
 *  @brief Start discovering systems
 *
 *  @discussion Scans for systems over the assigned protocol. You should implement the TAPSystemServiceDelegate protocol [TAPSystemServiceDelegate didDiscoverSystem:advertisementData:RSSI:]
 *  @see    [TAPSystemServiceDelegate didDiscoverSystem:advertisementData:RSSI:]
 */
- (void)scanForSystems;

/*!
 *  @method scanForSystems
 *
 *  @param options      An optional dictionary specifying options for the scan.
 *
 *  @brief Start discovering systems
 *
 *  @discussion Scans for systems over the assigned protocol. You should implement the TAPSystemServiceDelegate protocol [TAPSystemServiceDelegate didDiscoverSystem:advertisementData:RSSI:]
 *  @see    [TAPSystemServiceDelegate didDiscoverSystem:advertisementData:RSSI:]
 */
- (void)scanForSystemsWithOptions:(NSDictionary*)options;

/*!
 *  @method stopScanForSystems
 *
 *  @brief Stop discovering systems
 *
 *  @discussion Stops scanning for systems over the assigned protocol.
 */
- (void)stopScanForSystems;

/*!
 *  @method connectSystem:
 *
 *  @param system   The target system
 *
 *  @brief Connects to target system
 *
 *  @discussion You should implement the TAPSystemServiceDelegate protocol [TAPSystemServiceDelegate didConnectToSystem:success:error:]
 *  @see    [TAPSystemServiceDelegate didConnectToSystem:success:error:]
 */
- (void)connectSystem:(id)system;

/*!
 *  @method disconnectSystem:
 *
 *  @param system   The target system
 *
 *  @brief Disconnects to target system
 *
 *  @discussion You should implement the TAPSystemServiceDelegate protocol [TAPSystemServiceDelegate didDisconnectToSystem:error:]
 *  @see    [TAPSystemServiceDelegate didDisconnectToSystem:error:]
 */
- (void)disconnectSystem:(id)system;

/*!
 *  @method system:startPairing:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief The classic bluetooth of the target system start pairing
 *
 *  @discussion Device will trigger bluetooth pairing and auto disconnect BLE
 */
- (void)system:(id)system startPairing:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:stopPairing:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief The classic bluetooth of the target system stop pairing
 *
 */
- (void)system:(id)system stopPairing:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:startReconnecting:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief The classic bluetooth of the target system start reconnecting
 *
 */
- (void)system:(id)system startReconnecting:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:stopReconnecting:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief The classic bluetooth of the target system stop reconnecting
 *
 */
- (void)system:(id)system stopReconnecting:(void (^)(NSDictionary*))complete;

/*!
*  @method system:powerCableConnectionStatus:
*
*  @param system           The target system
*  @param connectionStatus The block which taking power cable connection status as a parameter
*
*  @brief The power cable connection status of the target system
*
*/
- (void)system:(id)system powerCableConnectionStatus:(void (^)(TAPSystemServicePowerCableConntectionStatus))connectionStatus;

/*!
 *  @method startMonitorPowerCableConnectionStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the power cable connection status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE power cable connection charateristic provided by the system in system service. Whenever there's a power cable connection status change, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPSystemService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPSystemService](#) , [TAPSystemServiceDelegate system:didUpdatePowerCableConnectionStatus:] will be triggered and provide the response data.
 *
 *  @see [TAPSystemServiceDelegate system:didUpdatePowerCableConnectionStatus:]
 */
- (void)startMonitorPowerCableConnectionStatusOfSystem:(id)system;

/*!
 *  @method stopMonitorPowerCableConnectionStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the power cable connection status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the power cable connection status charateristic provided by the system service.
 *  @see    startMonitorPowerCableConnectionStatusOfSystem:
 */
- (void)stopMonitorPowerCableConnectionStatusOfSystem:(id)system;

/*!
 *  @method system:pairingStatus:
 *
 *  @param system           The target system
 *  @param pairingStatus The block which taking pairing status as a parameter
 *
 *  @brief The pairing mode status of the target system
 *
 */
- (void)system:(id)system pairingStatus:(void (^)(TAPSystemServicePairingStatus))pairingStatus;

/*!
 *  @method startMonitorPairingModeStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the pairing mode status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE pairing mode  charateristic provided by the system in system service. Whenever there's a pairing mode status change, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPSystemService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPSystemService](#) , [TAPSystemServiceDelegate system:didUpdatePairingModeStatus:] will be triggered and provide the response data.
 *
 *  @see [TAPSystemServiceDelegate system:didUpdatePairingModeStatus:]
 */
- (void)startMonitorPairingModeStatusOfSystem:(id)system;

/*!
 *  @method stopMonitorPairingModeStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the pairing mode status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the pairing mode status charateristic provided by the system in the service.
 *  @see    stopMonitorPairingModeStatusOfSystem:
 */
- (void)stopMonitorPairingModeStatusOfSystem:(id)system;



/*!
 *  @method system:enterDFUMode:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
*
 *  @brief                  Send enter DFU command to target system
 *
 *
 */
- (void)system:(id)system enterDFUMode:(void (^)(NSDictionary*))complete;

/*!
 *  @method deviceIdentifierCustomData:
 *
 *  @param customData       The device identifier custom data
 *
 *  @brief                  This property maintains some user specified information from the app.
 *
 *  @discussion App can define the data pattern by its own. This will be used to store device color and location data provided by the user. The device should persist this data but does not need to process it in any way. The default value should be 0x00
 */
- (void)system:(id)system deviceIdentifierCustomData:(void (^)(NSData*))customData;

/*!
 *  @method writeDeviceIdentifierCustomData:completion:
 *
 *  @param customData       The device identifier custom data, max 31 bytes
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Write custom data to the target system
 *
 *
 */
- (void)system:(id)system writeDeviceIdentifierCustomData:(NSData*)customData completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:MACAddress:
 *
 *  @param system           The target system
 *  @param address          The block which taking the MAC address as a parameter
 *
 *  @brief                  Reads the MAC address from the target system
 *
 *
 */
- (void)system:(id)system MACAddress:(void (^)(NSData*))address;

/*!
 *  @method system:trueWirelessPairedDeviceMACAddress:
 *
 *  @param system           The target system
 *  @param address          The block which taking the MAC address of the trueWireless paried device as a parameter
 *
 *  @brief                  Reads the MAC address of the trueWireless paired device from the target system
 *
 *  @discussion             This method is only valid when trueWireless is connected
 */
- (void)system:(id)system trueWirelessPairedDeviceMACAddress:(void (^)(NSData*))address;

/*!
 *  @method system:features:
 *
 *  @param system           The target system
 *  @param features         The block which taking the feature  list as a parameter
 *
 *  @brief                  Reads the feature list from the target system
 *
 *
 */
- (void)system:(id)system features:(void (^)(NSArray*))features;


/*!
 *  @method system:deviceName:
 *
 *  @param system           The target system
 *  @param deviceName       The block which taking the device name as a parameter
 *
 *  @brief                  Reads the model number from the target system
 *
 *  @discussion             Device Name (UUID 2A00). Read only utf8 string. From the factory this property should be set to the marketing name of the product. This characteristic should be writable but due to limitations in iOS it is not. We work around this by adding a custom writable characteristic that is [TAPSystemService system:writeCustomName:completion:]
 
     The spec for generic access is available at: https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.generic_access.xml
 *  @see                    system:writeCustomName:completion:
 */
- (void)system:(id)system deviceName:(void (^)(NSString*))deviceName;

/*!
 *  @method system:customName:
 *
 *  @param system           The target system
 *  @param customName       The block which taking the custom name as a parameter
 *
 *  @brief                  Reads the custom name from the target system
 *  @see                    system:deviceName:
 *
 */
- (void)system:(id)system customName:(void (^)(NSString*))customName;

/*!
 *  @method system:writeCustomName:completion:
 *
 *  @param system           The target system
 *  @param customName       The target custom name. In BLE communication, max length of name can be 4 or 17 bytes, determined by BLE service UUID length.
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the custom name to the target system
 *  @see                    system:deviceName:
 */
- (void)system:(id)system writeCustomName:(NSString*)customName completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:modelNumber:
 *
 *  @param system           The target system
 *  @param modelNumber      The block which taking the model number as a parameter
 *
 *  @brief                  Reads the model number from the target system
 *  @discussion             The model number should be the non-user facing identifier uniquely representing the product model. It will be used by the app to identify what graphical icon should represent the product. This is what we calls the type number
 */
- (void)system:(id)system modelNumber:(void (^)(NSString*))modelNumber;

/*!
 *  @method system:serialNumber:
 *
 *  @param system           The target system
 *  @param serialNumber     The block which taking the serial number as a parameter
 *
 *  @brief                  Reads the serial number from the target system
 *  @discussion             The serial number as printed on the product. The app will use this to identify devices it has previously been connected with and for product registration purposes
 *
 */
- (void)system:(id)system serialNumber:(void (^)(NSString*))serialNumber;

/*!
 *  @method system:hardwareVersion:
 *
 *  @param system           The target system
 *  @param version         	The block which taking the hardware version as a parameter
 *
 *  @brief                  Reads the hardware version from the target system
 *
 *
 */
- (void)system:(id)system hardwareVersion:(void (^)(NSString*))version;

/*!
 *  @method system:softwareVersion:
 *
 *  @param system           The target system
 *  @param version         	The block which taking the software version as a parameter
 *
 *  @brief                  Reads the software version from the target system
 *  @discussion             Should strictly follow a format (TBA) like: “X.Y.Z (yymmdd) – e.g. "0.4.4 (150216)”
 *
 */
- (void)system:(id)system softwareVersion:(void (^)(NSString*))version;

/*!
 *  @method system:productName:
 *
 *  @param system           The target system
 *  @param name         	The block which taking the product name as a parameter
 *
 *  @brief                  Reads the product name from the target system
 *
 *
 */
- (void)system:(id)system productName:(void (^)(NSString*))name;

/*!
 *  @method system:batteryLevel:
 *
 *  @param system           The target system
 *  @param batteryLevel     The block which taking the batteryLevel as a parameter
 *
 *  @brief                  Reads the batteryLevel from the target system
 *
 *
 */

- (void)system:(id)system batteryLevel:(void (^)(NSNumber*))batteryLevel;

/*!
 *  @method startMonitorBatteryStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the battery status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE battery charateristic provided by the system in system service. Whenever there's a battery change, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPSystemService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPSystemService](#) , [TAPSystemServiceDelegate system:didUpdateBatteryStatus:] will be triggered and provide the response data.
 *
 *  @see [TAPSystemServiceDelegate system:didUpdateBatteryStatus:]
 */
- (void)startMonitorBatteryStatusOfSystem:(id)system;

/*!
 *  @method stopMonitorBatteryStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the battery status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE battery status charateristic provided by the system in system service.
 *  @see  startMonitorBatteryStatusOfSystem:
 */
- (void)stopMonitorBatteryStatusOfSystem:(id)system;

/*!
 *  @method system:locationName:
 *
 *  @param system           The target system
 *  @param locationName     The block which taking the location name as a parameter
 *
 *  @brief                  Reads the location number from the target system
 *
 *
 */
- (void)system:(id)system locationName:(void (^)(NSString*))locationName;//TODO: to be implemented, current TYM platform has not supported yet

/*!
 *  @method system:writeLocationName:completion:
 *
 *  @param system           The target system
 *  @param locationName     The target location name
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the location name to the target system
 *
 *
 */
- (void)system:(id)system writeLocationName:(NSString*)locationName completion:(void (^)(NSDictionary*))complete;//TODO: to be implemented, current TYM platform has not supported yet

/*!
 *  @method system:color:
 *
 *  @param system           The target system
 *  @param color            The block which taking the color as a parameter
 *
 *  @brief                  Reads the color from the target system
 *
 *
 */
- (void)system:(id)system color:(void (^)(UIColor*))color;//TODO: to be implemented, current TYM platform has not supported yet

/*!
 *  @method system:customColor:
 *
 *  @param system           The target system
 *  @param customColor      The block which taking the custom color as a parameter
 *
 *  @brief                  Reads the custom color from the target system
 *
 *
 */
- (void)system:(id)system customColor:(void (^)(UIColor*))customColor;//TODO: to be implemented, current TYM platform has not supported yet

/*!
 *  @method system:writeCustomColor:completion:
 *
 *  @param system           The target system
 *  @param customColor      The target custom color
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the custom color to the target system
 *
 *
 */
- (void)system:(id)system writeCustomColor:(UIColor*)customColor completion:(void (^)(NSDictionary*))complete;//TODO: to be implemented, current TYM platform has not supported yet

/*!
 *  @method system:powerStatus:
 *
 *  @param system           The target system
 *  @param status           The block which taking the power status as a parameter
 *
 *  @brief                  Reads the power status of the target system
 *  @see                    TAPSystemServicePowerStatus
 *
 */
- (void)system:(id)system powerStatus:(void (^)(TAPSystemServicePowerStatus))status;

/*!
 *  @method system:writeLocationName:completion:
 *
 *  @param system           The target system
 *  @param status           The target power status to turn to
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Turns the target system to the target power status
 *  @see                    TAPSystemServicePowerStatus
 *
 */
- (void)system:(id)system turnPowerStatus:(TAPSystemServicePowerStatus)status completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorPowerStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the power status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE power status charateristic provided by the system in system service. Whenever there's a power status change, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPSystemService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPSystemService](#) , [TAPSystemServiceDelegate system:didUpdateBatteryStatus:] will be triggered and provide the response data.
 *
 *  @see [TAPSystemServiceDelegate system:didUpdateBatteryStatus:]
 */
- (void)startMonitorPowerStatusOfSystem:(id)system;

/*!
 *  @method stopMonitorPowerStatusOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the battery status of the target system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE power status charateristic provided by the system in system service.
 *  @see  startMonitorPowerStatusOfSystem:
 */
- (void)stopMonitorPowerStatusOfSystem:(id)system;

/*!
 *  @method system:autoOffTimer:
 *
 *  @param system           The target system
 *  @param interval         The block which taking the duration of the timer as a parameter
 *
 *  @brief                  Reads the current time of the auto-off timer of the target system
 *
 *  @discussion Duration is in minutes. Value 0 means auto-off timer is closed.

 */
- (void)system:(id)system autoOffTimer:(void (^)(NSInteger))duration;

/*!
 *  @method system:closeAutoOffTimer:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Close auto-off timer of the target system
 *
 */
- (void)system:(TAPSystem*)system closeAutoOffTimer:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:setAutoOffTimer:completion:
 *
 *  @param system           The target system
 *  @param duration         The duration of auto-off timer in minutes
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Set auto-off timer of the target system
 *
 */
- (void)system:(TAPSystem*)system setAutoOffTimer:(NSInteger)duration completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method startMonitorAutoOffTimerOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Starts monitoring the current time of the Auto off timer of the system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App subscribes to the BLE auto off timer  charateristic provided by the system in system service. Whenever there's a time update triggered, the system will send a BLE notification to the App, where the TAP protocol module will receive and notify [TAPSystemService](#) object. If you have implemented the delegate method and assigned to the delegate property of [TAPSystemService](#) , [TAPSystemServiceDelegate system:didUpdateAutoTimer:] will be triggered and provide the response data.
 *
 *  @see [TAPSystemServiceDelegate system:didUpdateAutoTimer:]
 */
- (void)startMonitorAutoOffTimerOfSystem:(id)system;

/*!
 *  @method stopMonitorAutoOffTimerOfSystem:
 *
 *  @param system   The target system
 *
 *  @brief  Stops monitoring the current time of the Auto off timer of the system.
 *
 *  @discussion On BLE common mode, calling this method will trigger the App unsubscripe to the BLE auto off timer charateristic provided by the system in system service.
 *  @see  startMonitorAutoOffTimerOfSystem:
 */
- (void)stopMonitorAutoOffTimerOfSystem:(id)system;

/*!
 *  @method system:settings:
 *
 *  @param system           The target system
 *  @param settings         The dictionary encapsulates all the settings
 *
 *  @brief                  Read the settings of the target system
 *
 *
 */
- (void)system:(TAPSystem*)system settings:(void (^)(NSDictionary*, NSError*))settings;

/*!
 *  @method system:writeSettingsType:value:completion:
 *
 *  @param system           The target system
 *  @param type             The type of the settings
 *  @param value            The value of the settings
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Configue one type of settings with a target value of the target system
 *
 *
 */
- (void)system:(TAPSystem*)system writeSettingsType:(NSString*)type value:(id)value completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:settings:
 *
 *  @param system           The target system
 *  @param presetId         The ID of the target preset
 *  @param presetName       The name of the target settings
 *
 *  @brief                  Read the name of the target preset of the target system
 *
 *
 */
- (void)system:(TAPSystem*)system preset:(NSString*)presetId name:(void (^)(NSString*))presetName;

/*!
 *  @method system:preset:writeName:completion:
 *
 *  @param system           The target system
 *  @param presetId         The ID of the target preset
 *  @param name             The name to be written
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Configue name of the target preset of the target system
 *
 *
 */
- (void)system:(TAPSystem*)system preset:(NSString*)presetId writeName:(NSString*)name completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method revertAllPresetNamesOfSystem:completion:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Revert the name of all the presets of the target system
 *
 */
- (void)revertAllPresetNamesOfSystem:(TAPSystem*)system completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:loadToCurrentFromPreset:completion:
 *
 *  @param system           The target system
 *  @param presetId         The ID of the target preset
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Loading the preset value to the system
 *
 *
 */
- (void)system:(TAPSystem*)system loadToCurrentFromPreset:(NSString*)presetId completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:saveCurrentToPreset:completion:
 *
 *  @param system           The target system
 *  @param presetId         The ID of the target preset
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Save the current value of the system to the target preset
 *
 *
 */
- (void)system:(TAPSystem*)system saveCurrentToPreset:(NSString*)presetId completion:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:toggleScreen:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Toggle the display screen
 *
 *
 */
- (void)system:(TAPSystem*)system toggleScreen:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:factoryReset:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Factory reset the target system
 */
- (void)system:(id)system factoryReset:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:displayLock:
 *
 *  @param system           The target system
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Lock the display
 *
 *
 */
- (void)system:(TAPSystem*)system displayLock:(void (^)(NSDictionary*))complete;

/*!
 *  @method system:brightness:
 *
 *  @param system           The target system
 *  @param brightness       The block which taking the brightness as a parameter
 *
 *  @brief                  Reads the brightness from the target system
 *
 *
 */
- (void)system:(id)system brightness:(void (^)(NSInteger))brightness;

/*!
 *  @method system:writeBrightness:completion:
 *
 *  @param system           The target system
 *  @param brightness       The target brightness, range in [0,255]
 *  @param complete         The completion block, use key TAPServiceCompletionKeyError to extract the possible error
 *
 *  @brief                  Writes the brightness to the target system
 *
 *  @discussion On BLE, the complete block will return the BLE response. An NSError object otherwise.
 */
- (void)system:(id)system writeBrightness:(NSInteger)brightness completion:(void (^)(NSDictionary*))complete;

@end

/*!
 *  @protocol TAPSystemServiceDelegate
 *
 *  @brief    Delegate define methods for receive notifications from TAPSystemService object.
 *
 */
@protocol TAPSystemServiceDelegate <NSObject>

@required

/*!
 *  @method didUpdateState:
 *
 *  @param state        The state of the system service
 *
 *  @brief              Invoked when there is an udpate of the state of the systemService
 *
 *  @discussion You should only start using the system service when you receive the TAPSystemServiceStateReady. Different protocol will need to take different steps to initial the system service. For example, BLE will need to wait for the bluetooth radio power on. This delegate function will notify you all these steps completed and the system service are ready to be used.
 *  @see                TAPSystemServiceState
 */
- (void)didUpdateState:(TAPSystemServiceState)state;

@optional

/*!
 *  @method didDiscoverDevice:advertisementData:RSSI
 *
 *  @param system       The discovered system
 *  @param advertisementData         The advertisment data of the system
 *  @param RSSI         The RSSI of the system
 *
 *  @brief              Invoked when a new system has been discovered
 *  @discussion Use keys TAPSystemKeyAdvertisementDataIsConnectable, TAPSystemKeyAdvertisementDataLocalName, TAPSystemKeyAdvertisementDataManufacturerData, to extract the advertisementData. Reference to CBAdvertisementData.
 */
- (void)didDiscoverSystem:(id)system advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI;

/*!
 *  @method didConnectToSystem:success:error:
 *
 *  @param system       The target system
 *  @param success      The connection result
 *  @param error        The connection error
 *
 *  @brief              Invoked when connection operation to the target system is finished
 *  @discussion         Return the connection result no matter it's successful or failed, which is indicated by the success flag. The connection can be successful with or without an error object. This depends on what we received from the system.
 */
- (void)didConnectToSystem:(id)system success:(BOOL)success error:(NSError*)error;

/*!
 *  @method didDisconnectToSystem:error:
 *
 *  @param system       The target system
 *  @param error        The disconnection error
 *
 *  @brief              Invoked when disconnection operation to the target system is finished
 */
- (void)didDisconnectToSystem:(id)system error:(NSError*)error;

/*!
 *  @method didUpdateSystem:
 *
 *  @param system       The target system
 *
 *  @brief              Invoked when there's an update to the target system
 */
- (void)didUpdateSystem:(id)system;


/*!
 *  @method system:didUpdateBatteryStatus:
 *
 *  @param system       The system which has this update
 *  @param batteryLevel The new battery level
 *
 *  @brief              Invoked when a the system has a battery update
 */
- (void)system:(id)system didUpdateBatteryStatus:(NSNumber*)batteryLevel;

/*!
 *  @method system:didUpdatePowerStatus:
 *
 *  @param system       The system which has this update
 *  @param status       The new power status
 *
 *  @brief              Invoked when a the system has a power status update
 *  @see                TAPSystemServicePowerStatus
 */
- (void)system:(id)system didUpdatePowerStatus:(TAPSystemServicePowerStatus)status;

/*!
 *  @method system:didUpdateSpeakerLinkConfig:
 *
 *  @param system       The system which has this update
 *  @param config       The new SpeakerLink config
 *
 *  @brief              Invoked when a the system has a SpeakerLink update
 */
- (void)system:(id)system didUpdateSpeakerLinkConfig:(NSDictionary*)config;

/*!
 *  @method system:didUpdatePairingModeStatus:
 *
 *  @param system       The system which has this update
 *  @param status       The new pairing mode status
 *
 *  @brief              Invoked when a the system has a pairing mode status update
 *  @see                TAPSystemServicePairingStatus
 */

- (void)system:(id)system didUpdatePairingModeStatus:(TAPSystemServicePairingStatus)status;

/*!
 *  @method system:didUpdatePowerCableConnectionStatus:
 *
 *  @param system       The system which has this update
 *  @param status       The new power cable connection status
 *
 *  @brief              Invoked when a the system has a power cable connection status update
 *  @see                TAPSystemServicePowerCableConntectionStatus
 */
- (void)system:(id)system didUpdatePowerCableConnectionStatus:(TAPSystemServicePowerCableConntectionStatus)status;

/*!
 *  @method system:didUpdateAutoSleepTimer:
 *
 *  @param system       The system which has this update
 *  @param batteryLevel The current time value of the auto sleep timer
 *
 *  @brief              Invoked when received an update of the auto sleep timer
 */
- (void)system:(id)system didUpdateAutoSleepTimer:(NSNumber*)SleepTimer;

/*!
 *  @method system:didReceiveError:
 *
 *  @param system        The system which has this update
 *  @param error         The received system error
 *
 *  @brief               Notifies if any errors received from the system
 */
- (void)system:(id)system didReceiveError:(NSError*)error;

/*!
*  @method system:connectionEventDidOccur:
*
*  @param system        The system which has this update
*  @param connectionInfo The connection info dictionary
*
*  @brief               Notifies if a connection event occurred which matches the registered options.
*  @code
      CBConnectionEvent event = [[connectionInfo objectForKey:TAPProtocolKeyConnectionEvent] integerValue];
*  @endcode
*/
- (void)system:(id)system connectionEventDidOccur:(NSDictionary*)connectionInfo;

@end
